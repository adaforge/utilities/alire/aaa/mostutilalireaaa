--  Configuration for tests generated by Alire
pragma Restrictions (No_Elaboration_Code);
pragma Style_Checks (Off);

package mostutilalireaaa_Tests_Config is
   pragma Pure;

   Crate_Version : constant String := "0.0.0";
   Crate_Name : constant String := "mostutilalireaaa_tests";

   Alire_Host_OS : constant String := "macos";

   Alire_Host_Arch : constant String := "x86_64";

   Alire_Host_Distro : constant String := "distro_unknown";

   type Build_Profile_Kind is (release, validation, development);
   Build_Profile : constant Build_Profile_Kind := development;

   type Trace_Level_Kind is (Info, Details, Debug);
   Trace_Level : constant Trace_Level_Kind := Info;

end mostutilalireaaa_Tests_Config;
