# AAA

Alex's Ada Assortment of Utilities

- Text formatting (paragraphs, tables).
- Indefinite containers.
- Enumeration validity/conversions of string images.
- On-demand stack backtraces.
- String containers and utilities.
- Simple subprocess spawning.
- Write-through minimal cache for objects stored in files
