--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: LGPL-3.0-or-later
--  SPDX-FileCopyrightText: Copyright 2022 Alejandro R. Mosteo (alejandro@mosteo.com)
--  SPDX-Creator: Alejandro R. Mosteo (alejandro@mosteo.com)
--  --------------------------------------------------------------------------------------
--  Initial creation date : 2018-06-11
--  --------------------------------------------------------------------------------------
--
--  @summary
--
--  @description
--
--  --------------------------------------------------------------------------------------
pragma License (Modified_GPL);

package AAA.Containers with Pure is

end AAA.Containers;
