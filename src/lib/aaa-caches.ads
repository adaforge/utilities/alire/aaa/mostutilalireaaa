--  --------------------------------------------------------------------------------------
--  SPDX-License-Identifier: LGPL-3.0-or-later
--  SPDX-FileCopyrightText: Copyright 2022 Alejandro R. Mosteo (alejandro@mosteo.com)
--  SPDX-Creator: Alejandro R. Mosteo (alejandro@mosteo.com)
--  --------------------------------------------------------------------------------------
--  Initial creation date : 2018-06-11
--  --------------------------------------------------------------------------------------
--
--  @summary
--
--  @description
--
--  --------------------------------------------------------------------------------------
pragma License (Modified_GPL);

package AAA.Caches with Preelaborate is

   type Cache is limited interface;

   procedure Discard (This : in out Cache) is abstract with
     Post'Class => not This.Has_Element;
   --  Discard the cached value and force a reload on next use

   function Has_Element (This : Cache) return Boolean is abstract;
   --  Say if there is a cached value

end AAA.Caches;
